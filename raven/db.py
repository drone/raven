import os

import peewee
import sqlite3


db = peewee.SqliteDatabase(os.environ.get('DB_PATH', 'raven.db'),
                           detect_types=sqlite3.PARSE_DECLTYPES)
