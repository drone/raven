import click

from .db import db
from .models import Client, Grant, Token, User
from .server import app


@app.cli.command()
def initdb():
    """Initialize the database."""
    print('Init the db')
    db.create_tables([Token, User, Client, Grant], safe=True)


@app.cli.command()
@click.argument('token')
def dummytoken(token):
    """Create a dummy token for dev."""
    Token.delete().where(Token.access_token == token).execute()
    Token.create(user=User.get(), access_token=token, expires_in=3600 * 24,
                 token_type='Bearer', scope='*')
    print('Created token', token)


@app.cli.command()
@click.option('--username', prompt='username')
@click.option('--email', prompt='email')
@click.option('--password', prompt='password')
def createuser(username, email, password):
    """Create a user."""
    user = User(username=username, email=email)
    user.set_password(password)
    user.save()
    print('Created', user)


@app.cli.command()
def listusers():
    """List registered users with details."""
    tpl = '{:<20} {}'
    print(tpl.format('username', 'email'))
    for user in User.select():
        print(tpl.format(user.username, user.email))


@app.cli.command()
@click.option('--name', prompt='name')
@click.option('--user', prompt='user')
def createclient(name, user):
    """Create a client."""
    user_inst = User.select().where(
        (User.username == user) | (User.email == user)).first()
    if not user_inst:
        print('User not found', user)
        return
    client = Client(name=name, user=user_inst)
    client.save()
    print('Created', client)


@app.cli.command()
def listclients():
    """List existing clients with details."""
    tpl = '{:<40} {:<40} {}'
    print(tpl.format('name', 'client_id', 'client_secret'))
    for client in Client.select():
        print(tpl.format(client.name, str(client.client_id),
                         client.client_secret))
