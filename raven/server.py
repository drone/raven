import logging
import os
from datetime import datetime
from uuid import UUID

from flask import (Flask, abort, g, jsonify, make_response, render_template,
                   request)
from flask_cors import CORS
from flask_oauthlib.provider import OAuth2Provider
from werkzeug.datastructures import ImmutableMultiDict

from . import models

app = Flask(__name__)
app.config.update(
    SECRET_KEY=os.environ.get('SECRET_KEY', 'sikr3t'),
)
CORS(app, supports_credentials=True)

logger = logging.getLogger('moose')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)

auth = OAuth2Provider(app)


def is_uuid4(uuid_string):
    """
    Validate that a UUID string is in
    fact a valid uuid4.
    """
    try:
        UUID(uuid_string, version=4)
    except (ValueError, TypeError):
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
        # None will raise TypeError.
        return False
    return True


def json_to_form(func):
    def wrapper(*args, **kwargs):
        if not request.form:
            request.form = ImmutableMultiDict(request.json)
        return func(*args, **kwargs)
    return wrapper


@auth.clientgetter
def clientgetter(client_id):
    # FIXME Allow direct token access for dev with email/pwd
    if not is_uuid4(client_id):
        return False
    return models.Client.select().where(
        models.Client.client_id == client_id).first()


@auth.usergetter
def usergetter(username, password, client, req):
    user = models.User.select().where(models.User.username == username).first()
    if user and user.check_password(password):
        return user
    return None


@auth.tokengetter
def tokengetter(access_token=None, refresh_token=None):
    if access_token:
        token = models.Token.select().where(
            models.Token.access_token == access_token).first()
        if token:
            # We use TZ aware datetime while Flask Oauthlib wants naive ones.
            # We cannot parse the timezone given that it is not returned in
            # ISO format: +00:00 vs. +0000
            without_tz = token.expires[:-6]
            expires = datetime.strptime(without_tz, '%Y-%m-%d %H:%M:%S.%f')
            token.expires = expires
            return token


@auth.tokensetter
def tokensetter(metadata, req, *args, **kwargs):
    # req: oauthlib.Request (not Flask one).
    metadata.update(dict(req.decoded_body))
    metadata['client'] = models.Client.get(client_id=req.client_id)
    token = models.Token(**metadata)
    token.save()
    if not token:
        abort(400, error='Missing payload')


@auth.grantgetter
def grantgetter(client_id, code):
    if not is_uuid4(client_id):
        return False
    return models.Grant.select().where(
        models.Grant.client.client_id == client_id,
        models.Grant.code == code).first()


@auth.grantsetter
def grantsetter(client_id, code, request, *args, **kwargs):
    # Needed by flask-oauthlib, but not used by client_crendentials flow.
    pass


@app.before_request
def load_current_user():
    user = models.User.select().get()
    g.user = user


@app.route('/home')
def home():
    return render_template('home.html')


@app.route('/oauth/authorize', methods=['GET', 'POST'])
@auth.authorize_handler
def authorize(*args, **kwargs):
    # NOTICE: for real project, you need to require login
    if request.method == 'GET':
        # render a page for user to confirm the authorization
        return render_template('confirm.html')

    if request.method == 'HEAD':
        # if HEAD is supported properly, request parameters like
        # client_id should be validated the same way as for 'GET'
        response = make_response('', 200)
        response.headers['X-Client-ID'] = kwargs.get('client_id')
        return response

    confirm = request.form.get('confirm', 'no')
    return confirm == 'yes'


@app.route('/oauth/token', methods=['POST', 'GET'])
@auth.token_handler
def access_token():
    return {}


@app.route('/oauth/revoke', methods=['POST'])
@auth.revoke_handler
def revoke_token():
    pass


@app.route('/key', methods=['PUT'])
@auth.require_oauth('key')
def key_provisioning():
    oauth = request.oauth
    # username = oauth.user.username
    # TODO: stores key et hutch.
    # TODO: generate AES key.
    return jsonify(secret='bar')


@auth.invalid_response
def require_oauth_invalid(req):
    return jsonify(message=req.error_message), 401
