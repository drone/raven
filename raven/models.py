import random
import string
import uuid
from datetime import datetime, timedelta, timezone

import peewee
from playhouse.fields import PasswordField

from .db import db


def utcnow():
    return datetime.now(timezone.utc)


def generate_secret(size=55, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class User(peewee.Model):
    identifiers = ['email']
    resource_fields = ['username', 'email', 'company']

    username = peewee.CharField(max_length=100, index=True)
    email = peewee.CharField(max_length=100, unique=True)
    company = peewee.CharField(max_length=100, null=True)
    password = PasswordField(null=True)
    is_staff = peewee.BooleanField(default=False, index=True)

    class Meta:
        database = db

    def __str__(self):
        return self.username

    def set_password(self, password):
        self.password = password
        self.save()

    def check_password(self, password):
        return self.password.check_password(password)


class Client(peewee.Model):
    identifiers = ['client_id']
    resource_fields = ['name', 'user']

    GRANT_AUTHORIZATION_CODE = 'authorization_code'
    GRANT_CLIENT_CREDENTIALS = 'client_credentials'
    GRANT_TYPES = (
        (GRANT_AUTHORIZATION_CODE, 'Authorization code'),
        (GRANT_CLIENT_CREDENTIALS, 'Client credentials'),
    )
    default_scopes = ['key']
    client_id = peewee.UUIDField(unique=True, default=uuid.uuid4)
    name = peewee.CharField(max_length=100)
    user = peewee.ForeignKeyField(User)
    client_secret = peewee.CharField(unique=True, max_length=55)
    redirect_uris = peewee.CharField()
    grant_type = peewee.CharField(choices=GRANT_TYPES)

    class Meta:
        database = db

    def __str__(self):
        return self.name

    @property
    def default_redirect_uri(self):
        return self.redirect_uris[0] if self.redirect_uris else None

    @property
    def allowed_grant_types(self):
        return [id for id, name in self.GRANT_TYPES]

    def save(self, *args, **kwargs):
        if not self.client_secret:
            self.client_secret = generate_secret()
            self.redirect_uris = ['http://localhost/authorize']  # FIXME
            self.grant_type = self.GRANT_CLIENT_CREDENTIALS
        super().save(*args, **kwargs)


class Grant(peewee.Model):
    user = peewee.ForeignKeyField(User, null=True)
    client = peewee.ForeignKeyField(Client, null=True)
    code = peewee.CharField(max_length=255, index=True, null=False)
    redirect_uri = peewee.CharField()
    scope = peewee.CharField(null=True)
    expires = peewee.DateTimeField()

    class Meta:
        database = db

    @property
    def scopes(self):
        return self.scope.split() if self.scope else None


class Token(peewee.Model):
    user = peewee.ForeignKeyField(User, null=True)
    client = peewee.ForeignKeyField(Client, null=True)
    token_type = peewee.CharField(max_length=40)
    access_token = peewee.CharField(max_length=255)
    refresh_token = peewee.CharField(max_length=255, null=True)
    scope = peewee.CharField(max_length=255)
    expires = peewee.DateTimeField()

    class Meta:
        database = db

    def __init__(self, **kwargs):
        expires_in = kwargs.pop('expires_in', 60 * 60)
        kwargs['expires'] = utcnow() + timedelta(seconds=expires_in)
        super().__init__(**kwargs)

    @property
    def scopes(self):
        # TODO: custom charfield
        return self.scope.split() if self.scope else None

    def is_valid(self, scopes=None):
        """
        Checks if the access token is valid.
        :param scopes: An iterable containing the scopes to check or None
        """
        return not self.is_expired() and self.allow_scopes(scopes)

    def is_expired(self):
        """
        Check token expiration with timezone awareness
        """
        return utcnow() >= self.expires

    def allow_scopes(self, scopes):
        """
        Check if the token allows the provided scopes
        :param scopes: An iterable containing the scopes to check
        """
        if not scopes:
            return True

        provided_scopes = set(self.scope.split())
        resource_scopes = set(scopes)

        return resource_scopes.issubset(provided_scopes)
